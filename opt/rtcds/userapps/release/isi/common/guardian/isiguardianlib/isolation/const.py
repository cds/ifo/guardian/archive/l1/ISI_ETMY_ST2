from .. import const as top_const

DC_BIAS_DEVICE = dict(
        HAM = 'CPS',
        BSC_ST1 = 'CPS',
        BSC_ST2 = 'CPS',
        HPI = 'IPS',
    )

# Default definition of the various isolation levels
#
# INDEX is the state index of the ultimate full-isolated state 
#
# MAIN is list of filter banks to be turned on during main phase
#
# BOOST is list of filter banks tobe turnedon during boost phase
#
DEFAULT_LEVEL_FILTERS = {
    'HIGH': {
        'INDEX': 400,
        'MAIN': ('FM4', 'FM5', 'FM6', 'FM7',),
        'BOOST': ('FM8',),
        },
    'MEDIUM': {
        'INDEX': 300,
        'MAIN': ('FM2', 'FM3',),
        'BOOST': ('FM9',),
        },
    'ROBUST': {
        'INDEX': 200,
        'MAIN': ('FM1',),
        'BOOST': ('FM10',),
        },
    }

# Master dictionary that defines default values for all settable
# isolation parameters for HAM, BSC_ST1, BSC_ST2, and HPI:
#
#   ALL_DOF: full list of DOFs to be isolated
#
#   DOF_LISTS: tuple of lists of DOFs, describing states of isolation:
#   tuple[0] list will be isolated first, then tuple[1], etc.
#
#   CART_BIAS_DOF_LISTS: tuple of lists of DOFs of cartesian bias
#   offsets to be restored.  len(tuple) should be the same as
#   DOF_LISTS (above), and each element should be a list containing a
#   subset of the DOFs listed in the same element of DOF_LISTS.
#
#   CART_BIAS_RESTORE_OFFSET_RAMP_TIME: ramp time for cart bias
#   restores
#
#   CART_BIAS_RESTORE_POST_WAIT_TIME: wait time after cart bias
#   restore.  This is a kind of hacky way to wait for things to settle
#   before moving to the next isolation stage.
#
#   RAMP_*_GAINS: list of gains for potentially multi-step isolation
#   filter ramp stage.  If the list includes more than one element,
#   the isolation gains will be ramped in len() steps.
#   RAMP_*_TIMES: list of gain ramp times for isolation filters gain
#   ramps (see RAMP_*_GAINS above)
#
#   ISO_OK_SENSOR: a sensor name (e.g. 'T240') with corresponding
#   monitor channel <ISO_OK_SENSOR>+'_MONITOR_OUT'.  If this field is
#   present, a state will be created (WAIT_FOR_<ISO_OK_SENSOR>_SETTLE)
#   that will prevent the isolation loops from engaging until the
#   monitor == 0.
#
#   ONLY_ON_BUTTONS: 
#
#   LEVEL_FILTERS: dictionary of isolation level names and the filter
#   banks to be engaged during MAIN and BOOST phases (dict of dicts).
#   This dictionary determines which "level branches" are added to the
#   system graph: e.g. one branch for each key in the dictionary.
#   Essentially three different levels are supported (see
#   DEFAULT_LEVEL_FILTERS above).  More commonly, though, we would
#   remove from LEVEL_FILTERS the levels we don't want to support in
#   the graph.
#
ISOLATION_CONSTANTS_ALL = dict(
        HAM = dict(
            ALL_DOF = top_const.DOF_LIST,
            DOF_LISTS = (['Z', 'RX', 'RY'], ['X', 'Y', 'RZ']),
            CART_BIAS_DOF_LISTS = (['Z', 'RX', 'RY'], ['X', 'Y', 'RZ']),
            GS13_GAIN_DOF = ['H1', 'H2', 'H3', 'V1', 'V2','V3'],
            SWITCH_GS13_GAIN = False,
            FF_DOF = dict(
                FF = [],
                HPI_FF = [],
            ),
            CART_BIAS_RESTORE_OFFSET_RAMP_TIME = 15.,
            CART_BIAS_RESTORE_POST_WAIT_TIME = 0.,
            RAMP_UP_GAINS = [0.01, 1.],
            RAMP_UP_TIMES = [10., 10.],
            RAMP_DOWN_GAINS = [0.1, 0.],
            RAMP_DOWN_TIMES = [5., 5.],
            ONLY_ON_BUTTONS = ['INPUT', 'OUTPUT', 'DECIMATE'],
            LEVEL_FILTERS = {'HIGH': DEFAULT_LEVEL_FILTERS['HIGH']},
        ),
        BSC_ST1 = dict(
            ALL_DOF = top_const.DOF_LIST,
            DOF_LISTS = (['Z', 'RX', 'RY'], ['X', 'Y', 'RZ']),
            CART_BIAS_DOF_LISTS = ([], ['RZ']),
            SWITCH_GS13_GAIN = False,
            FF_DOF = dict(
                FF01 = [],
                ST2_DRIVE_COMP = [],
            #    FF12 = [],
            #    FF12_C = [],
            ),
            CART_BIAS_RESTORE_OFFSET_RAMP_TIME = 5.,
            CART_BIAS_RESTORE_POST_WAIT_TIME = 0.,
            RAMP_UP_GAINS = [0.01, 1.],
            RAMP_UP_TIMES = [10., 10.],
            RAMP_DOWN_GAINS = [0.1, 0.],
            RAMP_DOWN_TIMES = [5., 5.],
            ISO_OK_SENSOR = 'T240',
            ONLY_ON_BUTTONS = ['INPUT', 'OUTPUT', 'DECIMATE'],
            LEVEL_FILTERS = {'HIGH': DEFAULT_LEVEL_FILTERS['HIGH']},
        ),
        BSC_ST2 = dict(
            ALL_DOF = top_const.DOF_LIST,
            DOF_LISTS = (['Z', 'RX', 'RY'], ['X', 'Y', 'RZ']),
            CART_BIAS_DOF_LISTS = ([], ['RZ']),
            GS13_GAIN_DOF = ['H1', 'H2', 'H3', 'V1', 'V2','V3'],
            SWITCH_GS13_GAIN = False,
            FF_DOF = dict(
                FF12 = [],
            ),
            CART_BIAS_RESTORE_OFFSET_RAMP_TIME = 5.,
            CART_BIAS_RESTORE_POST_WAIT_TIME = 0.,
            RAMP_UP_GAINS = [0.01, 1.],
            RAMP_UP_TIMES = [10., 10.],
            RAMP_DOWN_GAINS = [0.1, 0.],
            RAMP_DOWN_TIMES = [5., 5.],
            ONLY_ON_BUTTONS = ['INPUT', 'OUTPUT', 'DECIMATE'],
            LEVEL_FILTERS = {'HIGH': DEFAULT_LEVEL_FILTERS['HIGH']},
        ),
        HPI = dict(
            ALL_DOF = top_const.DOF_LIST,
            DOF_LISTS = (['Z', 'RX', 'RY', 'VP'], ['X', 'Y', 'RZ', 'HP']),
            CART_BIAS_DOF_LISTS = ([], ['Z','RX','RY','VP','X','Y','RZ','HP']),
            GS13_GAIN_DOF = ['H1', 'H2', 'H3', 'V1', 'V2','V3'],
            SWITCH_GS13_GAIN = False,
            FF_DOF = dict(
                FF = [],
                HPI_FF = [],
            ),
            CART_BIAS_RESTORE_OFFSET_RAMP_TIME = 30.,
            CART_BIAS_RESTORE_POST_WAIT_TIME = 0.,
            RAMP_UP_GAINS = [0.01, 1.],
            RAMP_UP_TIMES = [15., 15.],
            RAMP_DOWN_GAINS = [0.1, 0.],
            RAMP_DOWN_TIMES = [15., 15.],
            ONLY_ON_BUTTONS = ['INPUT', 'OUTPUT', 'DECIMATE'],
            LEVEL_FILTERS = {'ROBUST': DEFAULT_LEVEL_FILTERS['ROBUST']},
        ),
    )

ISOLATION_CONSTANTS = ISOLATION_CONSTANTS_ALL[top_const.CHAMBER_TYPE]
