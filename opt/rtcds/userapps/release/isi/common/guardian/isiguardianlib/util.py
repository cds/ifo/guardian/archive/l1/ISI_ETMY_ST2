# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
# SVN $Id$
# $HeadURL$

import operator
from functools import wraps, reduce

from ezca.ligofilter import Mask, LIGOFilter

from . import const
from .errormessage import ErrorMessage


class WrongArgument(Exception): pass


# check_arg() is a decorator that raises a WrongArgument exception if argument
# passed as the <arg_number> argument is not in the <allowed> arguments. This
# decorator is primarily intended to catch subtle bugs caused by bad argument
# passing.
def check_arg(arg_number, arg_name, allowed):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            try:
                if kwargs[arg_name] not in allowed:
                    raise WrongArgument("The {arg_name} argument of {func} must be in {allowed}. Passed {passed_arg}."\
                            .format(arg_name=arg_name, func=func.__name__, allowed=allowed, passed_arg=kwargs[arg_name]))
            except KeyError:
                if args[arg_number] not in allowed:
                    raise WrongArgument("The {nth} argument of {func} must be in {allowed}. Passed {passed_arg}."\
                            .format(nth=const.NTH[arg_number].lower(), func=func.__name__, allowed=allowed, passed_arg=args[arg_number]))
            return func(*args, **kwargs)
        return wrapper
    return decorator


def report_filter_error(error):
    notify(const.WRONG_FILTER_STATE_NOTIFICATION)
    log(error.message)


def report_exception(exception):
    notify('%s raised. See log.' % exception.__class__.__name__)
    log('%s raised: %s.' % (exception.__class__.__name__, str(exception)))


def get_loaded_filter_modules(ligo_filter, filter_module_list):
    loaded_filters = []
    for filter_module in filter_module_list:
        if ligo_filter.is_loaded(filter_module):
            loaded_filters.append(filter_module)
    return loaded_filters


# This function is similar to check_if_filters_are_in_requested_state(). The
# only difference is that it only checks that the loaded filters of those in
# <filter_module_names> are engaged.  If none of the filters in
# <filter_module_names> are loaded, a message is appended to <error_message>.
def check_if_requested_filter_modules_loaded(filter_channel_names, button_names, filter_module_names):
    error_message = ErrorMessage()
    for filter_name in filter_channel_names:
        ligo_filter = LIGOFilter(filter_name, ezca)
        loaded_filter_modules = get_loaded_filter_modules(ligo_filter, filter_module_names)
        requested_buttons = []
        if loaded_filter_modules:
            requested_buttons = button_names
        else:
            error_message += "%s has no filters loaded in %s." % (filter_name, filter_module_names)

        error_message += check_if_filters_are_in_requested_state([filter_name], requested_buttons, loaded_filter_modules,)

    return error_message


# Returns an <error_message> noting which filters in <filter_channel_names> do
# not have exactly the buttons and filters in <button_names> and <filter_names>
# engaged. An empty <error_message> implies that the filters match the state
# corresponding to <button_names> and <filter_names>
def check_if_filters_are_in_requested_state(filter_channel_names, button_names, filter_names):
    # Build mask for requested state corresponding to arguments
    expected_mask = Mask(button_names)
    expected_mask |= Mask(reduce(operator.add, map(lambda n: [n, n+'_ENGAGED'], filter_names), []))

    error_message = ErrorMessage()
    for filter_name in filter_channel_names:
        ligo_filter = LIGOFilter(filter_name, ezca)
        current_mask = ligo_filter.get_current_state_mask()
        if expected_mask != current_mask:
            error_message += const.WRONG_FILTER_STATE_ERROR_MSG.format(filter_name=filter_name,
                                                                 current_mask=current_mask,
                                                                 expected_mask=expected_mask)
    return error_message


def check_if_filters_have_correct_gain(filter_channel_names, correct_gain):
    error_message = ErrorMessage()
    for filter_name in filter_channel_names:
        ligo_filter = LIGOFilter(filter_name, ezca)
        gain = ligo_filter.GAIN.get()
        if gain != correct_gain:
            error_message += "Gain of %s: %.3f. Expected: %.3f."\
                    % (filter_name, gain, correct_gain)
    return error_message
